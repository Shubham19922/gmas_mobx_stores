import React from 'react';
import Api from 'gmas_networking'


export default class AddressItemController extends React.Component {

    static prodControllerInstance = null;

    constructor() {
        super();
    }

    static getInstance() {
        if (!this.prodControllerInstance) {
            this.prodControllerInstance = new AddressItemController();
        }
        return this.prodControllerInstance;

    }

    fetchAddress() {
        return Api.get(`user/get-address/?format=json`)
            .then(data => {
                return data;
            })
    }

    updateAddress(id, data) {
        return Api.get('user/get-address/?format=json/' + id + '/')
            .then(data => {
                return data;
            })
    }
}