import React from 'react';
import Api from 'gmas_networking'
import Constants from "../lib/Constants";


export default class OrderItemController extends React.Component {

    static orderItemController = null;

    constructor() {
        super();
    }

    static getInstance() {
        if (!this.orderItemController) {
            this.orderItemController = new OrderItemController();
        }
        return this.orderItemController;

    }

    fetchOrder(id) {
        return Api.get(Constants.USERNAME + `/api/v4/product/?format=json&fields=i|t|d|p|dp&id=` + id)
            .then(data => {
                return data;
            })
    }

    fetchTrackOrder(email, orderId) {
        return Api.get(`track-order/?trackid=` + orderId + "&trackemail=" + email)
            .then(data => {
                console.log("track order data in controller", data);
                return data;
            })
    }
}