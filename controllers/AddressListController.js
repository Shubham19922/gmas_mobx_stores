import React from 'react';
import Api from 'gmas_networking'


export default class AddressListController extends React.Component {

    static addressControllerInstance = null;

    constructor() {
        super();
    }

    static getInstance() {
        if (!this.addressControllerInstance) {
            this.addressControllerInstance = new AddressListController();
        }
        return this.addressControllerInstance;

    }

    fetchAddresses() {
        return Api.get('user/get-address/?format=json')
            .then(data => {
                console.log("address ", data);
                return data;
            })
    }

}