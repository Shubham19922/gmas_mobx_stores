import React from 'react';
import Api from 'gmas_networking'
import Constants from "../lib/Constants";

export default class CartListController extends React.Component {

    static cartControllerInstance = null;

    constructor() {
        super();
    }

    static getInstance() {
        if (!this.cartControllerInstance) {
            this.cartControllerInstance = new CartListController();
        }
        return this.cartControllerInstance;

    }



    fetchWholeSaleDiscount(wholeSaleObject) {
        return new Promise((resolve, reject) => {

            console.log("Json stringify wholesale ", JSON.stringify(wholeSaleObject))

            return Api.post(`calculate-wholesale-discount/`, JSON.stringify(wholeSaleObject), Constants.JSON_DATA)
                .then(data => {
                    console.log('Wholesale api ka response ', data)
                    resolve(data);
                    return data;
                })

        })


    }

    fetchPromoDiscount(promoBody) {
        return new Promise((resolve, reject) => {

            console.log("Json stringify wholesale ", promoBody)

            return Api.post(`/promotions/run/?mn=` + Constants.USERNAME, promoBody, Constants.FORM_DATA)
                .then(data => {
                    console.log('Promo api ka response ', data)
                    resolve(data);
                    return data;
                })

        })


    }

}