import React from 'react';
import Api from 'gmas_networking'
import Constants from "../lib/Constants";


export default class ContactFormController extends React.Component {

    static contactFormController = null;

    constructor() {
        super();
    }

    static getInstance() {
        if (!this.contactFormController ) {
            this.contactFormController  = new ContactFormController();
        }
        return this.contactFormController ;

    }



    submitContactForm(email, name, subject, message) {
        return new Promise((resolve, reject) => {
            var details = {
                'email': email,
                'name': name,
                'subject':subject,
                'message':message
            };

            var formBody = [];
            for (var property in details) {
                var encodedKey = encodeURIComponent(property);
                var encodedValue = encodeURIComponent(details[property]);
                formBody.push(encodedKey + "=" + encodedValue);
            }
            formBody = formBody.join("&");


            return Api.post(`contactus/`, formBody, Constants.FORM_DATA)
                .then(data => {
                    resolve('yes');
                    return data;
                })

        })


    }

}