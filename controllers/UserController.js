import React from 'react';
import Api from 'gmas_networking'
import Constants from "../lib/Constants";
import {AsyncStorage} from 'react-native';


export default class UserController extends React.Component {

    static userController = null;

    constructor() {
        super();
    }

    static getInstance() {
        if (!this.userController) {
            this.userController = new UserController();
        }
        return this.userController;

    }

    fetchCSRFToken() {
        return Api.getCSRF(`user/login/?type=csrf`)
            .then(data => {
                return data;
            })
    }

    loginUserRequest(username, password) {

        var details = {
            'email': username,
            'password': password
        };

        var formBody = [];
        for (var property in details) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(details[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");


        return Api.post(`user/login/`, formBody, Constants.FORM_DATA)
            .then(data => {
                console.log("login api data ==>", data);

                global.storage.save({
                    key: 'loginState',
                    data:{
                        username: username,
                        userId: data.id,
                    }
                })
                
                return data;
            })
    }

    checkSubscriptionType(userId){
        return new Promise((resolve, reject) => {
            let urlToAppend = `user/api/v1/enduser-subscription/?format=json&enduser=${userId}&loc_currency=INR`

            return Api.get(urlToAppend).then((data) => {
                console.log("Subscription type ka response in controller ", data)

                global.storage.save({
                    key: 'endUserType',
                    data:{
                        endUserType: data[0].enduser_type
                    }
                })

                resolve(data)
            })
        })
        
    }

    fetchEndUserTypes() {
        return new Promise((resolve, reject) => {
            let urlToAppend = `user/api/v1/user-type/?format=json&loc_currency=INR`

            return Api.get(urlToAppend).then((data) => {
                console.log("User types ka controller mein response ", data)
                resolve(data)
            })
        })
        
    }

    socialAuthFacebook(deviceId, accessToken){

        
        console.log("Device id | access Token controller", deviceId, " | ", accessToken)
        
        var details = {
            'access_token': accessToken,
            'device_id': deviceId,
            
        };

        var formBody = [];
        for (var property in details) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(details[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");

        console.log("form body in social auth ", formBody)

        return Api.post(Constants.USERNAME + "/dapi/user/mobile-social-auth-facebook/", formBody, Constants.FORM_DATA)
            .then(data => {
                console.log("Social auth api data ==>", data);
                return data;
            })
    }

    afterFetchContact(deviceId, contact){
        console.log("Contact in after fetch contact controller ", contact)

        var details = {
            'contact': contact,
            'device_id': deviceId,
            
        };

        var formBody = [];
        for (var property in details) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(details[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");

        return Api.post(Constants.USERNAME + "/dapi/user/mobile-social-signup-complete/", formBody, Constants.FORM_DATA)
            .then(data => {
                console.log("Social contact auth api data ==>", data);
                return data;
            })

    }

    socialAuthGoogle(deviceId, accessToken){

        
        console.log("Device id | access Token controller", deviceId, " | ", accessToken)
        
        var details = {
            'id_token': accessToken,
            'device_id': deviceId,
            
        };

        var formBody = [];
        for (var property in details) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(details[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");

        console.log("form body in social auth ", formBody)

        return Api.post(Constants.USERNAME + "/dapi/user/mobile-social-auth-google/", formBody, Constants.FORM_DATA)
            .then(data => {
                console.log("Social auth google api data ==>", data);
                return data;
            })
    }

    

    editProfileRequest(firstName, lastName, contact, birthday) {

        var details = {
            'firstname': firstName,
            'lastname': lastName,
            'contact': contact,
            'birthday': birthday
        };

        var formBody = [];
        for (var property in details) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(details[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");


        return Api.post(`user/home/`, formBody, Constants.FORM_DATA)
            .then(data => {
                console.log("edit profile api data ==>", data);
                return data;
            })
    }

    fetchUserProfile() {
        return Api.get(`user/home/?format=json`)
            .then(data => {
                return data;
            })
    }

    signupUserRequest(firstname,lastname,email,phone,password, confirmpassword, enduserType) {
       console.log("signupUserRequest");
        var details = {
            'firstname': firstname,
            'lastname': lastname,
            'email': email,
            'contact': phone,
            'password': password,
            'confirm_password': confirmpassword
            //'enduser_type': enduserType,
        };

        var formBody = [];
        for (var property in details) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(details[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");


        return Api.post(`user/register/`, formBody, Constants.FORM_DATA)
            .then(data => {
                console.log("signup api data ==>", data);
                return data;
            })
    }


}