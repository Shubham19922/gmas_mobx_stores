import React from 'react';
import Api from 'gmas_networking'
import Constants from '../lib/Constants'
import UserStore from "../stores/UserStore";
import { AsyncStorage } from 'react-native';


export default class ProductItemController extends React.Component {

    static prodControllerInstance = null;

    constructor() {
        super();

    }

    static getInstance() {
        if (!this.prodControllerInstance) {
            this.prodControllerInstance = new ProductItemController();
        }
        return this.prodControllerInstance;

    }

    fetchProduct(id) {
        return Api.get(Constants.USERNAME + `/api/v4/product/?format=json&fields=bp|id|g|t|sd|d|sku|s|p|dp|oos|b|c|attrs_v|cats|ai|ail|pcat|f|rs|rv|opt1|opt2|opt3|optv1|optv2|optv3|i|i2|i3|i4|i5|i6|i7|i8|i9|i0|di|di2|di3|di4|di5|di6|di7|di7|di8|di9|di10|zi|zi2|zi3|zi4|zi5|zi6|zi7|zi8|zi9|zi10|moq|v|vk|kgp|n&id=` + id)
            .then(data => {
                console.log("PRODUCT DATA ", data)
                return data;
            })
    }

    fetchProductVariants(id) {
        return Api.get(Constants.USERNAME + `/api/v4/product/?format=json&fields=id|d|s|p|dp|opt1|opt2|opt3|optv1|optv2|optv3|sku|i|i2|i3|i4|i5|di|di2|di3|di4|di5|zi|zi2|zi3|zi4|zi5|moq|ia|f|bp|attrs_v|v|vk|kgp|n|sd|b|cats|c|t&limit=500&g=` + id)
            .then(data => {
                return data;
            })
    }

    setFavrouiteProduct(id, userId) {

        return new Promise((resolve, reject) => {
            console.log("fav data controller ", id);
            try {
                let urlToAppend = "v1/user/wish-list/" + userId + "/add-item/?format=json";
                console.log('url add to wishlist ', urlToAppend);
                return Api.putForWishlist(urlToAppend, { 'product_id': id }, Constants.JSON_DATA)
                    .then(data => {
                        resolve(data)
                        return data;
                    })

            } catch (error) {
                console.log(error)
            }

        })

    }

    fetchProductOptions(id) {

        let urlToAppend = `${Constants.USERNAME}/api/v4/product-option/?format=json&fields=o|v|ov&p=${id}`

        return new Promise((resolve, reject) => {
            Api.get(urlToAppend)
                .then((data) => {
                    resolve(data)
                    return data;
                })
                .catch((err) => {
                    reject(err)
                    return err;
                })
        })
    }

    shippingOptions(country, pincode, prodId){
        console.log("Check Delivery Controller ", country, pincode, prodId)
        var details = {
            'country': country,
            'pincode': pincode,
            'product_id' : prodId
        };

        var formBody = [];
        for (var property in details) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(details[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");

        return Api.post(`user/shop-shipping-options/`, formBody, Constants.FORM_DATA)
            .then(data => {
                console.log("shipping options api data ==>", data);
                return data;
            })

    }

    saveOptionFile(imageUrl) {
        console.log("item controller mein save file ka param ", imageUrl)
        const data = new FormData();
        let photo = { uri: imageUrl.uri }
        console.log("photo", photo)
        //data.append({file: imageUrl});
        data.append("file", {
            uri: imageUrl,
            name: "image",
            type: 'multipart/form-data'
        });

        let urToAppend = 'save-file/'

        return new Promise((resolve, reject) => {
            Api.post(urToAppend, data, Constants.MULTIPART_DATA).then((data) => {
                console.log("Save file ka response ", data)
                resolve(data)
                return data
            }).catch((err) => {
                reject(data)
                return data
                console.log("Save file ka error ", err)
            })
        })


    }
}