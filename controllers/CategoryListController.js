import React from 'react';
import Api from 'gmas_networking'
import Constants from "../lib/Constants";


export default class CategoryListController extends React.Component {

    static categoryListController = null;

    constructor() {
        super();
    }

    static getInstance() {
        if (!this.categoryListController) {
            this.categoryListController = new CategoryListController();
        }
        return this.categoryListController;

    }

    fetchCategories(nextUrl, type, mode) { // type -- featured / not-featured  mode -- withProduct / withoutProduct
        let urlToAppend;

        if(mode === 'withProduct'){
            if(nextUrl){
                urlToAppend = `/api/v4/category/${nextUrl}`
            }else {
                urlToAppend = `/api/v4/category/?format=json&a=true&limit=10&fields=n|id|i|f|d|next_url|prs|t|sc&prs_limit=5`
            }
        }

        if(mode === 'withoutProduct'){
            if(nextUrl){
                urlToAppend = `/api/v4/category/${nextUrl}`
            }else {
                urlToAppend = `/api/v4/category/?format=json&a=true&limit=10&fields=n|id|i|f|d|next_url|t`
            }
        }
        

        if(type === 'featured'){
            urlToAppend += '&f=true'
        }

        if(type === 'parent'){
            urlToAppend += '&pc=none'
        }
        
        
        return Api.get(Constants.USERNAME + urlToAppend)
            .then(data => {
                console.log("Category Data ", data)
                return data;
            })
    }

    fetchBannerCategories(){

        return new Promise((resolve, reject) => {
            return Api.get(Constants.USERNAME + `/api/v4/category/?format=json&a=true&limit=500&fields=n|id|i|f&f=true`)
                .then(data => {
                    console.log(" banner Category Data ", data)
                    resolve(data)
                    return data;
                }).catch(err => {
                    console.log(err)
                    reject(err)
                    return (err)
                })
        })
    }

    

    fetchSideMenuCategories() {
        return new Promise((resolve, reject) => {
            return Api.get(Constants.USERNAME + `/api/v4/category/?format=json&a=true&limit=1000&fields=n|id|i|f|next_url|a|d|pc|t`)
                .then(data => {
                    console.log(" Side menu Category Data ", data)
                    resolve(data)
                    return data;
                }).catch(err => {
                    console.log(err)
                    reject(err)
                    return (err)
                })
        })
    }
}