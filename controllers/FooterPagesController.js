import React from 'react';
import Api from 'gmas_networking'
import Constants from "../lib/Constants";


export default class FooterPagesController extends React.Component {

    static footerPagesController = null;

    constructor() {
        super();
    }

    static getInstance() {
        if (!this.footerPagesController) {
            this.footerPagesController = new FooterPagesController();
        }
        return this.footerPagesController;

    }

    fetchData() {
        return Api.get(Constants.USERNAME + '/api/data/storelegal/?format=json')
            .then(data => {
                return data;
            })
    }
}