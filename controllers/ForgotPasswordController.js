import React from 'react';
import Api from 'gmas_networking'
import Constants from "../lib/Constants";


export default class ForgotPasswordController extends React.Component {

    static forgotPasswordController = null;

    constructor() {
        super();
    }

    static getInstance() {
        if (!this.forgotPasswordController ) {
            this.forgotPasswordController  = new ForgotPasswordController();
        }
        return this.forgotPasswordController ;

    }



    submitForgotPasswordForm(email) {
        return new Promise((resolve, reject) => {
            var details = {
                'email': email
            };

            var formBody = [];
            for (var property in details) {
                var encodedKey = encodeURIComponent(property);
                var encodedValue = encodeURIComponent(details[property]);
                formBody.push(encodedKey + "=" + encodedValue);
            }
            formBody = formBody.join("&");


            return Api.post(`user/forgot-password/`, formBody, Constants.FORM_DATA)
                .then(data => {
                    resolve(data);
                    return data;
                })

        })


    }

}