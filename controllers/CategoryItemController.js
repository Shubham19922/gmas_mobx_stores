import React from 'react';
import Api from 'gmas_networking'
import Constants from "../lib/Constants";


export default class CategoryItemController extends React.Component {

    static categoryItemController = null;

    constructor() {
        super();
    }

    static getInstance() {
        if (!this.categoryItemController) {
            this.categoryItemController = new CategoryItemController();
        }
        return this.categoryItemController;

    }

    fetchCategory(catId) {
        return Api.get(Constants.USERNAME + `/api/v4/category/?format=json&a=true&bp=null&prs_limit=5&fields=n|id|i|f|a|d|prs|dp|p|t|sc&id=` + catId)
            .then(data => {
                console.log("Category Data ", data)
                return data;
            })
    }
}