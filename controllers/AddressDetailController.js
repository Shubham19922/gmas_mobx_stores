import React from 'react';
import Api from 'gmas_networking'
import Constants from "../lib/Constants";


export default class AddressDetailController extends React.Component {

    static addControllerInstance = null;

    constructor() {
        super();
    }

    static getInstance() {
        if (!this.addControllerInstance) {
            this.addControllerInstance = new AddressDetailController();
        }
        return this.addControllerInstance;

    }

    fetchCountries() {
        return Api.get(`api/region/v1/country/?format=json&limit=500`)
            .then(data => {
                console.log("country data in controller", data);
                return data;
            })
    }

    fetchStates(countrycode) {
        return Api.get(`/api/region/v1/subdivision/?format=json&limit=500&country=` + countrycode)
            .then(data => {
                console.log("state data in controller", data);
                return data;
            })
    }

    submitAddressDetail(firstName, lastName, address, city, state, country, pincode, phone) {
        return new Promise((resolve, reject) => {
            var details = {
                'firstname': firstName,
                'lastname': lastName,
                'contact':phone,
                'address':address,
                'city':city,
                'state':state,
                'country':country,
                'code':pincode
            };

            var formBody = [];
            for (var property in details) {
                var encodedKey = encodeURIComponent(property);
                var encodedValue = encodeURIComponent(details[property]);
                formBody.push(encodedKey + "=" + encodedValue);
            }
            formBody = formBody.join("&");


            return Api.post(`user/address/create/`, formBody, Constants.FORM_DATA)
                .then(data => {
                    resolve('yes'+ data);
                    return data;
                })

        })


    }

    updateAddressDetail(firstName, lastName, address, city, state, country, pincode, phone, addressId) {
        return new Promise((resolve, reject) => {
            var details = {
                'firstname': firstName,
                'lastname': lastName,
                'contact':phone,
                'address':address,
                'city':city,
                'state':state,
                'country':country,
                'code':pincode
            };

            var formBody = [];
            for (var property in details) {
                var encodedKey = encodeURIComponent(property);
                var encodedValue = encodeURIComponent(details[property]);
                formBody.push(encodedKey + "=" + encodedValue);
            }
            formBody = formBody.join("&");

            let addressUpdateUrl = 'user/address/' + addressId + '/'

            return Api.post(addressUpdateUrl, formBody, Constants.FORM_DATA)
                .then(data => {
                    resolve('address Update'+ data);
                    return data;
                })

        })


    }
}
