import React from 'react';
import Api from 'gmas_networking'


export default class OrderListController extends React.Component {

    static orderControllerInstance = null;

    constructor() {

        super();
    }

    static getInstance() {

        if (!this.orderControllerInstance) {
            this.orderControllerInstance = new OrderListController();
        }
        return this.orderControllerInstance;

    }

    fetchOrders() {
        return Api.get(`user/orders/?format=json&order_by=-created&loc_currency=INR`)
            .then(data => {
                return data;
            })
    }
}