import React from 'react';
import Api from 'gmas_networking'
import Constants from "../lib/Constants";


export default class BannerController extends React.Component {

    static bannerListController = null;

    constructor() {
        super();
    }

    static getInstance() {
        if (!this.bannerListController) {
            this.bannerListController = new BannerController();
        }
        return this.bannerListController;

    }

    fetchBanners() {
        return Api.get(Constants.USERNAME + `/api/v3/custom-template/?format=json`)
            .then(data => {
                console.log("Banner Data ", data)
                return data;
            })
    }

    getArrayFromBannerData(data) {
        var bannerArray = [];
        var bannerImage = "";

        if (data) {
            for (let i = 1; i <= 6; i++) {
                let key = 'si' + i;
                bannerImage = (data.hasOwnProperty(key) && data[key]) ? data[key] : '';
                if (bannerImage) {
                    bannerArray.push(bannerImage)
                }
            }

        }

        return bannerArray;
    }


}