import React from 'react';
import Api from 'gmas_networking'
import Constants from "../lib/Constants";


export default class ChangePasswordController extends React.Component {

    static changePasswordController = null;

    constructor() {
        super();
    }

    static getInstance() {
        if (!this.changePasswordController ) {
            this.changePasswordController  = new ChangePasswordController();
        }
        return this.changePasswordController ;

    }

    submitPasswordForm(currentPassword, newPassword, confirmPassword) {
        return new Promise((resolve, reject) => {
            var details = {
                'current_password': currentPassword,
                'new_password': newPassword,
                'confirm_password':confirmPassword
            };

            var formBody = [];
            for (var property in details) {
                var encodedKey = encodeURIComponent(property);
                var encodedValue = encodeURIComponent(details[property]);
                formBody.push(encodedKey + "=" + encodedValue);
            }
            formBody = formBody.join("&");


            return Api.post(`user/change-password/`, formBody, Constants.FORM_DATA)
                .then(data => {
                    resolve('yes' + data);
                    return data;
                })

        })


    }

}