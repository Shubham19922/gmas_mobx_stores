import React from 'react';
import Api from 'gmas_networking'
import Constants from "../lib/Constants";
import UserStore from "../stores/UserStore";
import {AsyncStorage} from 'react-native';


export default class ProductListController extends React.Component {

    static prodControllerInstance = null;


    constructor() {
        super();

        let userId = "";
    }

    static getInstance() {
        if (!this.prodControllerInstance) {
            this.prodControllerInstance = new ProductListController();
        }
        return this.prodControllerInstance;

    }

    fetchProducts(catId, nextUrl, limit) {

        let urlToAppend;

        if (nextUrl) {
            urlToAppend = `/api/v4/product/${nextUrl}`
        } else if (limit === 'limit') {
            urlToAppend = `/api/v4/product/?format=json&limit=20&order_by=id&fields=bp|id|g|t|sd|d|sku|s|p|dp|oos|b|c|attrs_v|cats|ai|ail|pcat|f|rs|rv|opt1|opt2|opt3|optv1|optv2|optv3|i|i2|i3|i4|i5|i6|i7|i8|i9|i0|di|di2|di3|di4|di5|di6|di7|di7|di8|di9|di10|zi|zi2|zi3|zi4|zi5|zi6|zi7|zi8|zi9|zi10|moq|v|vk|kgp|n&limit=4&bp=none&cats__in=` + catId
        }

        else {
            urlToAppend = `/api/v4/product/?format=json&limit=20&order_by=id&fields=bp|id|g|t|sd|d|sku|s|p|dp|oos|b|c|attrs_v|cats|ai|ail|pcat|f|rs|rv|opt1|opt2|opt3|optv1|optv2|optv3|i|i2|i3|i4|i5|i6|i7|i8|i9|i0|di|di2|di3|di4|di5|di6|di7|di7|di8|di9|di10|zi|zi2|zi3|zi4|zi5|zi6|zi7|zi8|zi9|zi10|moq|v|vk|kgp|n&bp=none&cats__in=` + catId
        }


        return Api.get(Constants.USERNAME + urlToAppend)
            .then(data => {
                return data;
            })
    }

    fetchSearchedProducts(searchString) {
        console.log("fetchSearchedProducts method " + searchString)
        return Api.get(Constants.USERNAME + `/dapi/search/product/?suggest=` + searchString + '&limit=50&bponly=true&active=true')
            .then(data => {
                console.log("fetchSearchedProducts Api call", data)
                return data;
            })
    }

    fetchFeaturedProducts(limit, nextUrl) {

        let urlToAppend;

        if (nextUrl) {
            urlToAppend = `/api/v4/product/${nextUrl}`
        } else {
            urlToAppend = limit ? `/api/v4/product/?format=json&limit=5&fields=bp|id|g|t|sd|d|sku|s|p|dp|oos|b|c|attrs_v|cats|ai|ail|pcat|f|rs|rv|opt1|opt2|opt3|optv1|optv2|optv3|i|i2|i3|i4|i5|i6|i7|i8|i9|i0|di|di2|di3|di4|di5|di6|di7|di7|di8|di9|di10|zi|zi2|zi3|zi4|zi5|zi6|zi7|zi8|zi9|zi10|moq|v|vk|kgp|n&f=true&bp=none`
                : `/api/v4/product/?format=json&bp=none&fields=bp|id|g|t|sd|d|sku|s|p|dp|oos|b|c|attrs_v|cats|ai|ail|pcat|f|rs|rv|opt1|opt2|opt3|optv1|optv2|optv3|i|i2|i3|i4|i5|i6|i7|i8|i9|i0|di|di2|di3|di4|di5|di6|di7|di7|di8|di9|di10|zi|zi2|zi3|zi4|zi5|zi6|zi7|zi8|zi9|zi10|moq|v|vk|kgp|n&f=true`
        }

        return new Promise((resolve, reject) => {
            return Api.get(Constants.USERNAME + urlToAppend)
                .then(data => {
                    console.log("Featured Products ", data)
                    resolve(data)
                    return data;
                }).catch(err => {
                    console.log(err)
                    reject(err)
                    return (err)
                })
        })
    }

    fetchLatestProducts(limit, nextUrl) {

        let urlToAppend;

        if (nextUrl) {
            urlToAppend = `/api/v4/product/${nextUrl}`
        } else {
            urlToAppend = limit ? `/api/v4/product/?format=json&limit=5&fields=bp|id|g|t|sd|d|sku|s|p|dp|oos|b|c|attrs_v|cats|ai|ail|pcat|f|rs|rv|opt1|opt2|opt3|optv1|optv2|optv3|i|i2|i3|i4|i5|i6|i7|i8|i9|i0|di|di2|di3|di4|di5|di6|di7|di7|di8|di9|di10|zi|zi2|zi3|zi4|zi5|zi6|zi7|zi8|zi9|zi10|moq|v|vk|kgp|n&bp=none`
                : `/api/v4/product/?format=json&bp=none&fields=bp|id|g|t|sd|d|sku|s|p|dp|oos|b|c|attrs_v|cats|ai|ail|pcat|f|rs|rv|opt1|opt2|opt3|optv1|optv2|optv3|i|i2|i3|i4|i5|i6|i7|i8|i9|i0|di|di2|di3|di4|di5|di6|di7|di7|di8|di9|di10|zi|zi2|zi3|zi4|zi5|zi6|zi7|zi8|zi9|zi10|moq|v|vk|kgp|n`
        }

        return new Promise((resolve, reject) => {
            return Api.get(Constants.USERNAME + urlToAppend)
                .then(data => {
                    console.log("Featured Products ", data)
                    resolve(data)
                    return data;
                }).catch(err => {
                    console.log(err)
                    reject(err)
                    return (err)
                })
        })
    }

    setUserId(id) {
        this.userId = id;
    }

    applyFilter(catId, filterQuery) {
        let url = `${Constants.USERNAME}/api/v4/product/?format=json&limit=10&grouped=true&fields=i|t|cats|bp|p|dp|di|sd|d&cats__in=${catId}${filterQuery}`

        return new Promise((resolve, reject) => {
            Api.get(url).then((data) => {
                console.log("Filter ka response ", data)
                resolve(data)
                return data
            }).catch((err) => {
                reject(err)
                return err
            })
        })
    }

    fetchWishlistProducts(id) {
        console.log("product list controller k andar userId ", id);
        console.log("fetchWishlistProducts method");

        let urlToAppend = Constants.USERNAME + "/api/v3/wish-list/" + id + "/?format=json&fields=i|cats|attrs_v|dp|di|opt1|opt2|opt3|sku|t|p|s|sd|d|f|ia|b|c|rs|rv|ld|bd|hd|i|i2|i3|i4|i5|di|di2|di3|di4|di5|zi|zi2|zi3|zi4|zi5|cr|md|so|moq";
        console.log("urlToAppend : " + urlToAppend);

        return Api.get(urlToAppend)
            .then(data => {
                console.log("fetchWishlistProducts Api call", data)
                return data;
            })

    }

    fetchAttributes(id) {
        return new Promise((resolve, reject) => {
            Api.get(Constants.USERNAME + '/api/v4/attribute-key/?format=json&fields=n|f|ft|so|kgp|kvs|cats|v|vsc&kgp__gcats__in=' + id)
                .then(data => {
                    console.log("Attributes Data in controller ", data)
                    resolve(data)
                }).catch(err => {
                reject(err)
            })
        })
    }
}