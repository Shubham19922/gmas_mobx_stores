
import {Platform} from 'react-native';
import * as ToastAndroid from "react-native/Libraries/Components/ToastAndroid/ToastAndroid.android";

export default class Utils {

    static setImageWithPlaceholder(image) {

        return image === null ? require('../assets/images/placeholder.png') : {uri: image};
    }

    capitalizeString(str){
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    static showToast(message) {


        ToastAndroid.show(message, ToastAndroid.SHORT);


    }
}