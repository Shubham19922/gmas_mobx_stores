export  default Constants = {
    USERNAME : "grocery2home",
    APP_NAME : "G2H",
    PRIMARY_COLOR:'#ffffff',
    PRIMARY_COLOR_DARK:'#f3d400',
    SECONDARY_COLOR:'#f3f3f3',
    BASE_URI:'http://grocery2home.getmeashop.com/',
    BASE_URI_INVENTORY:'https://api.getmeashop.com/',
    JSON_DATA:'json',
    FORM_DATA:'formData',
    MULTIPART_DATA:'multipartData'
}


