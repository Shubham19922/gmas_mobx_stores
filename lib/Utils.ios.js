import * as AlertIOS from "react-native/Libraries/Alert/AlertIOS";
import { Alert } from "react-native";


export default class Utils {

    static setImageWithPlaceholder(image) {

        return image === null ? require('../assets/images/placeholder.png') : {uri: image};
    }

   static capitalizeString(str){
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    static showToast(message) {

        // AlertIOS.alert(
        //     message, message
        // );

        Alert.alert(
            'Notification',
             message,
            [
                {text: 'OK', onPress: () => console.log('ok Pressed!')},
            ],
            { cancelable: false }
        );



    }
}