import {action, observable} from 'mobx';
import BannerController from "../controllers/BannerController";

export class BannerStore {


    @observable id;
    @observable slider_image_1;
    @observable slider_image_2;
    @observable slider_image_3;
    @observable slider_image_4;
    @observable slider_image_5;
    @observable slider_image_6;
    @observable slider_image_7;
    @observable slider_image_8;
    @observable slider_image_9;
    @observable slider_image_10;
    @observable bannerData = [];
    @observable slider_1_type;


    constructor() {

        this.id = "";
        this.bannerData = [];

        //console.log("banner store call hua");
    }


    @action fetchBanners() {
        let controller = BannerController.getInstance();
        controller.fetchBanners().then(resp => {
            if (resp[0].si1) {
                if (resp[0].sl1) {
                    this.bannerData.push({image: resp[0].si1, nav: resp[0].sl1})
                } else {
                    this.bannerData.push({image: resp[0].si1, nav: null})
                }
            }

            if (resp[0].si2) {
                if (resp[0].sl2) {
                    this.bannerData.push({image: resp[0].si2, nav: resp[0].sl2})
                } else {
                    this.bannerData.push({image: resp[0].si2, nav: null})
                }
            }
            if (resp[0].si3) {
                if (resp[0].sl3) {
                    this.bannerData.push({image: resp[0].si3, nav: resp[0].sl3})
                } else {
                    this.bannerData.push({image: resp[0].si3, nav: null})
                }
            }
            if (resp[0].si4) {
                if (resp[0].sl4) {
                    this.bannerData.push({image: resp[0].si4, nav: resp[0].sl4})
                } else {
                    this.bannerData.push({image: resp[0].si4, nav: null})
                }
            }
            if (resp[0].si5) {
                if (resp[0].sl5) {
                    this.bannerData.push({image: resp[0].si5, nav: resp[0].sl5})
                } else {
                    this.bannerData.push({image: resp[0].si5, nav: null})
                }
            }
            if (resp[0].si6) {
                if (resp[0].sl6) {
                    this.bannerData.push({image: resp[0].si6, nav: resp[0].sl6})
                } else {
                    this.bannerData.push({image: resp[0].si6, nav: null})
                }
            }
            if (resp[0].si7) {
                if (resp[0].sl7) {
                    this.bannerData.push({image: resp[0].si7, nav: resp[0].sl7})
                } else {
                    this.bannerData.push({image: resp[0].si7, nav: null})
                }
            }
            if (resp[0].si8) {
                if (resp[0].sl8) {
                    this.bannerData.push({image: resp[0].si8, nav: resp[0].sl8})
                } else {
                    this.bannerData.push({image: resp[0].si8, nav: null})
                }
            }
            if (resp[0].si9) {
                if (resp[0].sl9) {
                    this.bannerData.push({image: resp[0].si9, nav: resp[0].sl9})
                } else {
                    this.bannerData.push({image: resp[0].si9, nav: null})
                }
            }
            if (resp[0].si10) {
                if (resp[0].sl10) {
                    this.bannerData.push({image: resp[0].si10, nav: resp[0].sl10})
                } else {
                    this.bannerData.push({image: resp[0].si10, nav: null})
                }
            }
        })
            .catch(err => console.log(err))
    }

}

export default new BannerStore();