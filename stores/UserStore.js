import {action, observable} from 'mobx';
import UserController from "../controllers/UserController";


export default class UserStore {

    static userStore = null;

    @observable id;
    @observable firstName;
    @observable lastName;
    @observable phone;
    @observable birthday;
    @observable gender;
    @observable referrer;
    @observable password;
    @observable confirmpassword;
    @observable email;
    @observable authenticationToken;
    @observable CSRFToken;
    @observable sessionId;
    @observable allEndUserTypes;


    constructor() {

        this.id = "";
        this.firstName = "";
        this.lastName = "";
        this.phone = "";
        this.birthday = "";
        this.password = "";
        this.confirmpassword = "",
            this.email = "";
        this.gender = "";
        this.referrer = "";
        this.authenticationToken = "";
        this.CSRFToken = "";
        this.sessionId = "";
        this.allEndUserTypes = [];
    }

    static getInstance() {
        if (!this.userStore) {
            this.userStore = new UserStore();
        }
        return this.userStore;

    }

    @action fetchEndUserTypes() {
        return new Promise((resolve, reject) => {
            let userController = UserController.getInstance()

            userController.fetchEndUserTypes().then(data => {
                console.log("User types ka stores mein response ", data)
                data.map((item) => {
                    this.allEndUserTypes.push({value: item.name, id: item.pk, isDefault: item.is_default})
                })
            })
        })
    }


    @action afterFetchContact(deviceId, contact) {
        return new Promise((resolve, reject) => {
            let userController = UserController.getInstance()

            userController.afterFetchContact(deviceId, contact).then((data) => {
                console.log("after fetch contact store ", data)
                resolve(data)
            })
        })
    }


    @action socialAuthFacebook(deviceId, accessToken) {


        console.log("Device id | access Token ", deviceId, " | ", accessToken)
        return new Promise((resolve, reject) => {
            let userController = UserController.getInstance();
            if (deviceId != null && accessToken != null) {

                userController.socialAuthFacebook(deviceId, accessToken).then(data => {
                    console.log("Social auth facebook ", data)
                    resolve(data)
                })
            }
        })


    }

    @action socialAuthGoogle(deviceId, accessToken) {

        console.log("Device id | access Token ", deviceId, " | ", accessToken)
        return new Promise((resolve, reject) => {
            let userController = UserController.getInstance();
            if (deviceId != null && accessToken != null) {

                userController.socialAuthGoogle(deviceId, accessToken).then(data => {
                    console.log("Social auth google ", data)
                    resolve(data)
                })
            }
        })

    }

    @action loginUser(username, password) {

        let userController = UserController.getInstance();
        return userController.loginUserRequest(username, password)
            .then(data => {
                console.log("data user ", data);

                if (data.id)
                    this.setId((data.id).toString());
                console.log("async k bad user ", data.id);
                console.log("async k bad user ",);

                global.storage.save({
                    key: 'loginState',
                    data: {
                        username: data.email,
                        userId: data.id,
                    }
                })
                return data;

            }).catch(err => {
                return err
            });
    }

    @action checkSubscriptionType(userId) {
        return new Promise((resolve, reject) => {
            let userController = UserController.getInstance()

            userController.checkSubscriptionType(userId).then((data) => {
                console.log("Subscription type ka data ", data)
            })
        })
    }

    @action editProfile(firstName, lastName, contact, birthday) {

        let userController = UserController.getInstance();
        return userController.editProfileRequest(firstName, lastName, contact, birthday)
            .then(data => {
                console.log("data user ", data);

                return data;

            });
    }


    @action signupUser(firstName, lastName, email, phone, password, confirmpassword, enduserType) {
        let userController = UserController.getInstance();
        return userController.signupUserRequest(firstName, lastName, email, phone, password, confirmpassword, enduserType).then(data => {
            console.log("data user ", data);

            return data;
        });
    }

    @action fetchCSRFToken() {
        let userController = UserController.getInstance();
        userController.fetchCSRFToken()
    }

    @action fetchUserProfile() {
        let userController = UserController.getInstance();
        return userController.fetchUserProfile().then(resp => {
            console.log("user data", resp);
            this.setFirstName(resp.firstname)
            this.setLastName(resp.lastname)
            this.setBirthday(resp.birthday)
            this.setPhone(resp.contact)

            console.log(this.firstName)


            return resp;

        })
        //.catch(err => console.log(err))
    }

    @action setData(data) {
        this.id = data.id;
        this.firstName = data.firstName;
        this.lastName = data.lastName;
        this.phone = data.phone;
        this.birthday = data.birthday;
        this.gender = data.gender;
        this.referrer = data.referrer;
        this.authenticationToken = data.authenticationToken;
        this.CSRFToken = data.CSRFToken;
        this.sessionId = data.sessionId;
        this.password = data.password;
        this.confirmpassword = data.confirmpassword;
        this.email = data.email;
    }

    @action setId(id) {
        this.id = id;
        console.log("id", this.id);
    }

    @action setFirstName(firstName) {
        this.firstName = firstName;
    }

    @action setLastName(lastName) {
        this.lastName = lastName;
    }

    @action setPhone(phone) {
        this.phone = phone;
    }

    @action setBirthday(birthday) {
        this.birthday = birthday;
    }

    @action setAuthenticationToken(authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    @action setCSRFToken(CSRFToken) {
        this.CSRFToken = CSRFToken;
    }

    @action setSessionId(sessionId) {
        this.sessionId = sessionId;
    }

    @action setEmail(email) {
        this.email = email;
    }

    @action setPassword(password) {
        this.password = password;
    }

    @action setConfirmPassword(confirmpassword) {
        this.confirmpassword = confirmpassword;
    }
}
