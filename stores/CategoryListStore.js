import {action, observable} from 'mobx';
import CategoryListController from "../controllers/CategoryListController";
import CategoryItemStore from "./CategoryItemStore";
import Utils from "../lib/Utils";
import SQLite from 'react-native-sqlite-storage'
import ProductListController from 'gmas_mobx_stores/controllers/ProductListController';

export class CategoryListStore {


    @observable categoryList;
    @observable sideMenuCategoryList;
    @observable bannerCategories;

    constructor() {
        this.categoryList = [];
        this.sideMenuCategoryList = [];
        this.nextUrl = '';
        this.bannerCategories = [];
    }


    @action updateCategoryList() {


        return new Promise((resolve, reject) => {
            let controller = CategoryListController.getInstance();
            controller.fetchCategories(this.nextUrl, null).then(resp => {

                console.log("Category list store ka response ", resp);

                this.setNextUrl(resp.meta.next);

                var i;
                for (i = 0; i < resp.objects.length; i++) {
                    let item = new CategoryItemStore();
                    item.setId(resp.objects[i].id)

                    if (resp.objects[i].i) {
                        item.setImageSource(resp.objects[i].i)
                    }
                    else {
                        item.setImageSource("http://www.independentmediators.co.uk/wp-content/uploads/2016/02/placeholder-image.jpg")
                    }
                    item.setTitle(resp.objects[i].n)
                    item.setDescription(resp.objects[i].d)
                    item.setFeatured(resp.objects[i].f)
                    item.setActive(resp.objects[i].a)
                    item.subProducts.push(resp.objects[i].prs)


                    console.log("Only products", item.subProducts);
                    this.categoryList.push(item);

                }

                resolve(true)
                return true
            })
                .catch(
                    reject(false)
                )
        })


    }

    @action fetchBannerCategories() {

        console.log("Banner Categories call hua hai ")

        this.bannerCategories = []

        let controller = CategoryListController.getInstance();
        controller.fetchBannerCategories().then((data) => {
            console.log("Banner Categories ", data)

            data.objects.map((item) => {
                if (item.i != null) {
                    console.log("banner categories map karte hue ", item)
                    this.bannerCategories.push(item)
                }
            })

            console.log("Banner categories push karne ke baad ", this.bannerCategories)
        })

    }

    @action fetchCategoryList(nextUrl, type, mode) { // type -- featured / not-featured  mode -- withProduct / withoutProduct
        if (this.categoryList.length > 0)
            return;


        this.categoryList = []

        let controller = CategoryListController.getInstance();
        controller.fetchCategories(nextUrl, type, mode).then(resp => {

            console.log("Category list store ka response ", resp);

            this.setNextUrl(resp.meta.next);

            var i;
            for (i = 0; i < resp.objects.length; i++) {
                let item = new CategoryItemStore();
                item.setId(resp.objects[i].id)

                if (resp.objects[i].i) {
                    item.setImageSource(resp.objects[i].i)
                }
                else {
                    item.setImageSource("http://www.independentmediators.co.uk/wp-content/uploads/2016/02/placeholder-image.jpg")
                }
                item.setTitle(resp.objects[i].n)
                item.setDescription(resp.objects[i].d)
                item.setFeatured(resp.objects[i].f)
                item.setActive(resp.objects[i].a)

                console.log("lol lol lol lol prs ", resp.objects[i].prs)

                item.subProducts.push(resp.objects[i].prs)


                console.log("Only products", item.subProducts);
                this.categoryList.push(item);

            }
        })
            .catch(err => console.log(err))
    }

    @action fetchSideMenuCategoryList() {

        return new Promise((resolve, reject) => {
            let controller = CategoryListController.getInstance();
            controller.fetchSideMenuCategories().then(resp => {

                console.log("side menu Category list store ka response ", resp);

                resp.objects.map((item) => {
                    this.sideMenuCategoryList.push(item);
                })

                const db = SQLite.openDatabase("enduser.db", "1.0", "Demo", -1);
                db.transaction((txn) => {
                    txn.executeSql('DROP TABLE IF EXISTS SideMenuCategories', []);
                    txn.executeSql('CREATE TABLE IF NOT EXISTS SideMenuCategories(NAME VARCHAR(30), ID  VARCHAR(10), PC_NAME VARCHAR(30) NULL, PC_ID  VARCHAR(10) NULL)', []);
                })


                resp.objects.map((item) => {
                    if (item.pc != null) {
                        db.transaction((txn) => {
                            txn.executeSql('INSERT INTO SideMenuCategories (NAME, ID, PC_NAME, PC_ID) VALUES (:NAME, :ID, :PC_NAME, :PC_ID)', [item.n, item.id, item.pc.n, item.pc.id]);

                        })
                    } else {
                        db.transaction((txn) => {
                            txn.executeSql('INSERT INTO SideMenuCategories (NAME, ID, PC_NAME, PC_ID) VALUES (:NAME, :ID, :PC_NAME, :PC_ID)', [item.n, item.id, null, null]);

                        })
                    }

                })
                resolve(true)
                return true
            })
                .catch(
                    reject(false)
                )
        })

    }

    @action setNextUrl(nextUrl) {
        this.nextUrl = nextUrl
    }

    @action getNextUrl() {
        return this.nextUrl
    }

    @action setCategoryList(categoryList) {
        this.categoryList = categoryList;
    }

    @action addCategoryItem(categoryItem) {
        this.categoryList.push(categoryItem);
    }

    @action removeCategoryItem(categoryItem) {
        let index = findIndex(categoryItem.id);

        if (index === -1) {
            return;
        }
        this.categoryList.remove(index);
    }

    @action updateProductItem(categoryItem) {
        let index = findIndex(categoryItem.id);

        this.categoryList[index].image = categoryItem.image;
        this.categoryList[index].title = categoryItem.title;
        this.categoryList[index].description = categoryItem.description;
        this.categoryList[index].featured = categoryItem.featured;
        this.categoryList[index].active = categoryItem.active;
    }

    findIndex(categoryId) {
        return this.categoryList.findIndex(categoryItem => {
            categoryItem.id === categoryId
        })
    }

}

export default new CategoryListStore();