import {action, observable} from 'mobx';
import OrderItemController from "../controllers/OrderItemController";
export default class OrderItemStore {


    @observable id;
    @observable address;
    @observable city;
    @observable code;
    @observable contact;
    @observable country;
    @observable created;
    @observable email;
    @observable firstname;
    @observable hash;
    @observable lastname;
    @observable orderJson;
    @observable orderStatus;
    @observable orderid;
    @observable paymentMethod;
    @observable paymentStatus;
    @observable state;
    @observable taxes;
    @observable totalOrder;


    constructor() {

        this.id = "";
        this.address;
        this.city;
        this.code;
        this.contact;
        this.country;
        this.created;
        this.email;
        this.firstname;
        this.hash;
        this.lastname;
        this.orderJson;
        this.orderStatus;
        this.orderid;
        this.paymentMethod;
        this.paymentStatus;
        this.state;
        this.taxes;
        this.totalOrder;
    }


    @action fetchOrder(id) {
        let controller = OrderItemController.getInstance();
        controller.fetchOrder(id).then(resp => {
            console.log("order data", resp);
            this.setId(resp[0].id);
            this.setAddress(resp[0].address);
            
            this.setFirstName(resp[0].firstname);
            this.setOrderJson(resp[0].order_json);
            this.setTotalOrder(resp[0].total_order);
            this.setPaymentMethod(resp[0].payment_method);
            this.setPaymentStatus(resp[0].payment_status);
            this.setCreated(resp[0].created);
            this.setContact(resp[0].contact);
            this.setState(resp[0].state)
            this.setCode(resp[0].code)
            this.setEmail(resp[0].email)
            this.setCity(resp[0].city)
            
        })
            .catch(err => console.log(err))
    }


    @action fetchOrderForTrack(email, orderId) {
        let controller = OrderItemController.getInstance();
        console.log("fetchOrderForTrack : " + email, orderId);

        return controller.fetchTrackOrder(email, orderId).then(resp => {
            console.log("order data", resp.order);
            this.setId(resp.order.id);
            this.setAddress(resp.order.address);
            this.setFirstName(resp.order.firstname);
            this.setOrderJson(resp.order.order_json);
            this.setTotalOrder(resp.order.total_order);
            this.setPaymentMethod(resp.order.payment_method);
            this.setPaymentStatus(resp.order.payment_status);
            this.setCreated(resp.order.created);
            this.setContact(resp.order.contact);
            this.setState(resp.order.state)
            this.setCode(resp.order.code)
            this.setCity(resp.order.city)
            this.setEmail(resp.order.email)

        })
            .catch(err => console.log(err))
    }

    @action setData(data) {
        this.id = data.id;
        this.image = data.image;
        this.price = data.price;
        this.description = data.description;
        this.discountPercentage = data.discountPercentage;
    }

    @action setId(id) {
        this.id = id;
    }

    @action setOrderId(id) {
        this.orderid = id;
    }
    
    @action setCity(city){
        this.city = city
    }

    @action setContact(contact){
        this.contact = contact
    }

    @action setState(state){
        this.state = state
    }

    @action setEmail(email){
        this.email = email
    }

    @action setCode(code){
        this.code = code
    }

    @action setAddress(address) {
        this.address = address;
    }

    @action setTotalOrder(totalOrder) {
        this.totalOrder = totalOrder;
    }

    @action setFirstName(firstname) {
        this.firstname = firstname;
    }

    @action setOrderJson(orderJson) {
        this.orderJson = orderJson;
    }

    @action setOrderStatus(orderStatus) {
        this.orderStatus = orderStatus;
    }

    @action setPaymentMethod(paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    @action setPaymentStatus(paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    @action setCreated(created) {
        this.created = created;
    }
}
