import { action, observable } from 'mobx';
import { AsyncStorage } from 'react-native';
export class CartStore {


    static cartStore = null;
    @observable productList = [];
    @observable totalPrice;
    @observable totalQuantity;
    @observable cartCount;

    constructor() {
        this.productList = [];
        this.totalPrice = 0;
        this.totalQuantity = 0;
        this.cartCount = 0;

        AsyncStorage.getItem("cartProducts").then(data => {
            console.log("cartdata ", data);
            if (data !== null) {
                this.productList = JSON.parse(data);
            }
        });

    }

    static getInstance() {
        if (!this.cartStore) {
            this.cartStore = new CartStore();
        }
        return this.cartStore;

    }

    @action setProductList(productList) {
        this.productList = productList;

    }

    @action setCartCount() {

        let cartData;
        let count = 0;

        AsyncStorage.getItem("cartProducts").then(data => {
            console.log("cartdata ", data);
            if (data !== null) {
                cartData = JSON.parse(data);
                //this.cartCount = this.productList.length
                for (i = 0; i < cartData.length; i++) {
                    count = count + cartData[i].quantity
                }
                console.log("Cart data parsed ", cartData)

                this.cartCount = count
            }

        });


    }

    @action addProductItem(productItem, qty) {
        let count = 0;
        console.log("Cart mein product item ", productItem)
        console.log("add item qty", qty)
        AsyncStorage.getItem("cartProducts").then(data => {
            console.log("cartdata ", data);
            if (data !== null) {
                
                this.productList = JSON.parse(data);
                
                console.log("Cart data parsed ", this.productList)
                
            }
            this.updateProdList(productItem, qty);


            for (i = 0; i < this.productList.length; i++) {
                count = count + this.productList[i].quantity
            }


            this.cartCount = count

            AsyncStorage.setItem("cartProducts", JSON.stringify(this.productList));

        });


    }


    @action addProductItemFromOrder(productList) {
        let count = 0;
        AsyncStorage.getItem("cartProducts").then(data => {
            console.log("cartdata ", data);
            if (data !== null) {
                
                this.productList = [];
                
                console.log("Cart data parsed ", this.productList)
                
            }

            productList.map((prod) => {
                this.updateProdList(prod.item, prod.itemQty);
            })
            


            for (i = 0; i < this.productList.length; i++) {
                count = count + this.productList[i].quantity
            }


            this.cartCount = count

            AsyncStorage.setItem("cartProducts", JSON.stringify(this.productList));

        });


    }


    @action removeProductItem(productId) {
        let count = 0;
        let index = this.findIndex(productId);
        console.log("index for remove prod ", index);
        if (index === -1) {
            return;
        }
        this.productList.splice(index, 1);
        console.log("prod list after remove ", this.productList.length);
        AsyncStorage.setItem("cartProducts", JSON.stringify(this.productList));

        for (i = 0; i < this.productList.length; i++) {
            count = count + this.productList[i].quantity
        }


        this.cartCount = count

    }

    @action updateProductItem(productItem) {
        console.log("inside update product", );
        let index = this.findIndex(productItem.id);

        this.productList[index].options = productItem.options
        this.productList[index].image = productItem.image;
        this.productList[index].price = productItem.price;
        this.productList[index].title = productItem.title;
        this.productList[index].description = productItem.description;
        this.productList[index].discountPrice = productItem.discountPrice;
        this.productList[index].opt1 = productItem.opt1;
        this.productList[index].opt2 = productItem.opt2;
        this.productList[index].opt3 = productItem.opt3;
        this.productList[index].optv1 = productItem.optv1;
        this.productList[index].optv2 = productItem.optv2;
        this.productList[index].optv3 = productItem.optv3;
        this.productList[index].stockKeepingUnit = productItem.stockKeepingUnit;
        this.productList[index].stock = productItem.stock

    }

    @action updateQuantity(prodId, qty) {
        let count = 0;

        this.productList.map((cartItem) => {
            if (cartItem.id === prodId) {
                cartItem.quantity = qty;
            }
        })



        AsyncStorage.setItem("cartProducts", JSON.stringify(this.productList));

        for (i = 0; i < this.productList.length; i++) {
            count = count + this.productList[i].quantity
        }


        this.cartCount = count
    }

    findIndex(productId) {
        console.log("inside find index");
        return this.productList.findIndex(productItem => {
            return productItem.id === productId
        })
    }


    updateProdList(productItem, qty) {
        console.log("add item qty", qty)
        console.log("this.findIndex(productItem.id) ", this.findIndex(productItem.id));
        if (this.findIndex(productItem.id) === -1) {
            this.productList.push(productItem);
            this.productList[this.findIndex(productItem.id)].quantity = this.productList[this.findIndex(productItem.id)].quantity + qty
        }
        else {
            this.productList[this.findIndex(productItem.id)].quantity
                = this.productList[this.findIndex(productItem.id)].quantity + qty;
        }
    }



}

export default CartStore.getInstance();