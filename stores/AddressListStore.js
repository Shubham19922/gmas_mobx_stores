import {action, observable} from 'mobx';
import AddressItemStore from "./AddressItemStore";
import AddressListController from "../controllers/AddressListController";

export class AddressListStore {


    @observable addressList;

    constructor() {
        this.addressList = [];

    }

    @action fetchAddressList() {
        this.addressList = [];


        return new Promise((resolve, reject) => {
            let controller = AddressListController.getInstance();
            controller.fetchAddresses().then(resp => {
                console.log("addresses new" + resp);
                resp = resp.addresses;
                var i;
                for (i = 0; i < resp.length; i++) {

                    console.log("addresses " + resp);

                    let item = new AddressItemStore();
                    item.setId(resp[i].id)
                    item.setFirstName(resp[i].firstname)
                    item.setLastName(resp[i].lastname)
                    item.setAddress(resp[i].address)
                    item.setCity(resp[i].city)
                    item.setCode(resp[i].code)
                    item.setCountry(resp[i].country)
                    item.setCountryDisplay(resp[i].country_display)
                    item.setContact(resp[i].contact)
                    item.setState(resp[i].state)
                    item.setStateDisplay(resp[i].state_display)
                    this.addressList.push(item);

                }
                console.log("addresses new" + resp);
                resolve(true)
                return true
            })
                .catch(
                    reject(false)
                )
        })

    }

    @action
        setAddressList(addressList)
        {
            this.addressList = addressList;
        }

    @action
        addAddressItem(addressItem)
        {
            this.addressList.push(addressItem);
        }

    @action
        removeProductItem(addressItem)
        {
            let index = findIndex(addressItem.id);

            if (index === -1) {
                return;
            }
            this.addressList.remove(index);
        }

    @action
        updateAddressItem(addressItem)
        {
            let index = findIndex(addressItem.id);

            this.addressList[index].address = addressItem.address;
            this.addressList[index].firstName = addressItem.firstName;
            this.addressList[index].lastname = addressItem.lastname;
            this.addressList[index].city = addressItem.city;
            this.addressList[index].code = addressItem.code;
            this.addressList[index].country = addressItem.country;
            this.addressList[index].countryDisplay = addressItem.countryDisplay;
            this.addressList[index].contact = addressItem.contact;
        }

        findIndex(itemId)
        {
            return this.addressList.findIndex(addressItem => {
                addressItem.id === itemId
            })
        }

    }

    export
    default
    new

    AddressListStore();