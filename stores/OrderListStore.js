import {action, observable} from 'mobx';
import OrderListController from "../controllers/OrderListController";
import OrderItemStore from "./OrderItemStore";

export class OrderListStore {


    @observable orderList;

    constructor() {
        this.orderList = [];
        console.log("orderliststore k andar");
    }

    @action fetchOrderList() {
        this.orderList = [];
        return new Promise((resolve, reject) => {
            let controller = OrderListController.getInstance();

            controller.fetchOrders().then(resp => {

                console.log("Order list resp ", resp)
                var i;
                for (i = 0; i < resp.length; i++) {

                    let item = new OrderItemStore();
                    item.setId(resp[i].id);
                    item.setOrderId(resp[i].orderid);
                    item.setAddress(resp[i].address);
                    item.setFirstName(resp[i].firstname);
                    item.setOrderJson(resp[i].order_json);
                    item.setTotalOrder(resp[i].total_order);
                    item.setOrderStatus(resp[i].order_status);
                    item.setPaymentMethod(resp[i].payment_method);
                    item.setPaymentStatus(resp[i].payment_status);
                    item.setCreated(resp[i].created);
                    item.setContact(resp[i].contact);
                    item.setState(resp[i].state)
                    item.setCode(resp[i].code)
                    item.setEmail(resp[i].email)
                    item.setCity(resp[i].city)

                    this.orderList.push(item);

                }
                resolve(true)
                return true
            })
                .catch(
                    reject(false)
                )
        })
    }


    @action setOrderList(orderList) {
        this.orderList = orderList;
    }

    @action addOrderList(orderItem) {
        this.orderList.push(orderItem);
    }

    @action removeOrderList(orderId) {
        let index = findIndex(orderId);

        if (index === -1) {
            return;
        }
        this.orderList.remove(index);
    }

    @action updateOrderItem(orderItem) {
        let index = findIndex(orderItem.id);

        this.orderList[index].firstname = orderItem.firstName;
        this.orderList[index].lastname = orderItem.lastname;
        this.orderList[index].totalOrder = orderItem.totalOrder;
        this.orderList[index].orderJson = orderItem.orderJson;
    }

    findIndex(orderId) {
        return this.orderList.findIndex(orderItem => {
            orderItem.id === orderId
        })
    }

}

export default new OrderListStore();