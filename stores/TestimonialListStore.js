import {action, observable} from 'mobx';
import ProductListController from '../controllers/ProductListController';
import ProductItemStore from './ProductItemStore';
import Utils from "../lib/Utils";

export class ProductListStore {


    @observable testimonialList;

    constructor() {
        this.testimonialList = [];

    }

    @action fetctTestimonialList(catId) {
        this.productList = [];
        let controller = ProductListController.getInstance();
        controller.fetchProducts(catId).then(resp => {

            var i;
            for (i = 0; i < resp.length; i++) {

                let item = new ProductItemStore();
                item.setId(resp[i].id)
                console.log(Utils.setImageWithPlaceholder(resp[i].i));
                item.setImageSource(Utils.setImageWithPlaceholder(resp[i].i))
                item.setTitle(resp[i].t)
                item.setDescription(resp[i].d)
                item.setPrice(resp[i].p)
                item.setDiscountPercentage(resp[i].dp)
                this.productList.push(item);

            }
        })
            .catch(err => console.log(err))
    }

    @action setProductList(productList) {
        this.productList = productList;
    }

    @action addProductItem(productItem) {
        this.productList.push(productItem);
    }

    @action removeProductItem(productId) {
        let index = findIndex(productId);

        if (index === -1) {
            return;
        }
        this.productList.remove(index);
    }

    @action updateProductItem(productItem) {
        let index = findIndex(productItem.id);

        this.productList[index].image = productItem.image;
        this.productList[index].price = productItem.price;
        this.productList[index].title = productItem.title;
        this.productList[index].description = productItem.description;
        this.productList[index].discountPrice = productItem.discountPrice;
    }

    findIndex(productId) {
        return this.productList.findIndex(productItem => {
            productItem.id === productId
        })
    }

}

export default new ProductListStore();