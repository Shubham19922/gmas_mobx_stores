import { action, observable } from 'mobx';
import CategoryItemController from 'gmas_mobx_stores/controllers/CategoryItemController';

export default class CategoryItemStore {


    @observable id;
    @observable image;
    @observable title;
    @observable description;
    @observable featured;
    @observable active;
    @observable subCategory;
    @observable subProducts;


    constructor() {

        this.id = "";
        this.image = "";
        this.title = "";
        this.description = "";
        this.featured = false;
        this.active = false;
        this.subCategory = [];
        this.subProducts = [];

    }

    @action fetchCategory(id) {
        console.log("Fetch Categories k andar")
        let controller = CategoryItemController.getInstance();

        controller.fetchCategory(id).then(resp => {

            console.log("response", resp);

            let data = [];

            let lengthFlag = resp.objects[0].sc.length;

            console.log("Yea vali length", lengthFlag);

            if (lengthFlag > 0) {
                for (j = 0; j < resp.objects[0].sc.length; j++) {

                    let item = new CategoryItemStore();
                    console.log("ID ", resp.objects[0].sc[j].id)
                    item.setId(resp.objects[0].sc[j].id)

                    if (resp.objects[0].sc[j].i) {
                        item.setImageSource(resp.objects[0].sc[j].i)
                    }
                    else {
                        item.setImageSource("http://www.independentmediators.co.uk/wp-content/uploads/2016/02/placeholder-image.jpg")
                    }
                    item.setTitle(resp.objects[0].sc[j].n)
                    item.setDescription(resp.objects[0].sc[j].d)
                    item.setFeatured(resp.objects[0].sc[j].f)
                    item.setActive(resp.objects[0].sc[j].a)


                    item.subProducts.push(resp.objects[0].sc[j].prs);

                    // item.setSubProductList(resp[0].sc[j].prs)
                    this.subCategory.push(item);
                }
            }

            else this.subCategory = null;

            console.log("Sub cat ", this.subCategory)
            this.title = "updated title"
        }).then(() => {
            console.log("")
        })
            .catch(err => console.log(err))
    }

    // @action setSubProductList(prs){
    //     console.log("product ka beta ", prs)
    //    this.subProducts.push(prs);
    //    console.log("product ka pota ", this.subProducts);
    // }

    @action setData(data) {
        this.id = data.id;
        this.image = data.image;
        this.price = data.price;
        this.description = data.description;
        this.discountPercentage = data.discountPercentage;
    }

    @action setId(id) {
        this.id = id;
    }

    @action setFeatured(featured) {
        this.featured = featured;
    }


    @action setImageSource(imageSource) {
        this.image = imageSource;
    }


    @action setTitle(title) {
        this.title = title;
    }

    @action setDescription(description) {
        this.description = description;
    }

    @action setActive(active) {
        this.active = active;
    }
}
