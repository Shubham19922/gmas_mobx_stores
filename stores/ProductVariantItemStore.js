import { action, observable } from 'mobx';
import ProductItemController from "../controllers/ProductItemController";
import Utils from '../lib/Utils';
import ProductItemStore from './ProductItemStore';

export default class ProductVariantItemStore {

    
    @observable productVariantList;
    @observable id;
    @observable image;
    @observable image2;
    @observable image3;
    @observable image4;
    @observable image5;
    @observable price;
    @observable title;
    @observable description;
    @observable discountPercentage;
    @observable quantity;
    @observable opt1;
    @observable opt2;
    @observable opt3;
    @observable optv1;
    @observable groupid;
    @observable attributeList;
    @observable stock;
    @observable stockKeepingUnit;
    @observable cats;
    @observable default;

    constructor() {

        this.id = "";
        this.image = "";
        this.image2 = "";
        this.image3 = "";
        this.image4 = "";
        this.image5 = "";
        this.price = -1;
        this.title = "";
        this.description = "";
        this.stock = 0;
        this.stockKeepingUnit = "";
        this.discountPrice = 0;
        this.quantity = 0;
        this.opt1 = "";
        this.opt2 = "";
        this.opt3 = "";
        this.groupid = "";
        this.productVariantList = [];
        this.attributeList = [];
        this.cats = ''
        this.default = false;


    }


    @action setAttributeList(attrs){
        if(attrs.length > 0){
            attrs.map((item) => {
                this.attributeList.push(item)
            })
        }
        
    }

    @action setId(id) {
        this.id = id;
    }

    @action setStockKeepingUnit(data){
        this.stockKeepingUnit = data
    }

    @action setStock(data){
        this.stock = data
    }

    @action setCats(cats){
        console.log("lol lol lol store mein cats set karte hue variant ", cats)
        this.cats = cats.join(',')
        console.log("lol lol lol store mein cats set karne ke baad variant ", this.cats)
    }

    @action setQuantity(quantity) {
        this.quantity = quantity;
    }


    @action setImageSource(imageSource) {
        this.image = imageSource;
    }

    @action setImage2Source(imageSource2) {
        this.image2 = imageSource2;
    }

    @action setImage3Source(imageSource3) {
        this.image3 = imageSource3;
    }

    @action setImage4Source(imageSource4) {
        this.image4 = imageSource4;
    }

    @action setImage5Source(imageSource5) {
        this.image5 = imageSource5;
    }

    @action setDefault(bool){
        this.default = bool
    }

    @action setPrice(price) {
        this.price = price;
    }

    @action setTitle(title) {
        this.title = title;
    }

    @action setDescription(description) {
        this.description = description;
    }

    @action setDiscountPrice(discount) {
        this.discountPrice = discount;
    }

    @action setOpt1(opt1) {
        this.opt1 = opt1;
    }

    @action setOpt2(opt2) {
        this.opt2 = opt2;
    }

    @action setOpt3(opt3) {
        this.opt3 = opt3;
    }

    @action setOptv1(optv1) {
        this.optv1 = optv1;
    }

    @action setOptv2(optv2) {
        this.optv2 = optv2;
    }

    @action setOptv3(optv3) {
        this.optv3 = optv3;
    }

    @action setGroupId(groupid) {
        this.groupid = groupid;
    }

}
