import {action, observable} from 'mobx';
import ProductListController from '../controllers/ProductListController';
import ProductItemStore from './ProductItemStore';
import Utils from "../lib/Utils";

export class ProductVariantListStore {


    @observable productVariantList;
    

    constructor() {
        this.productVariantList = [];
    }

    @action fetctProductList(catId) {
        this.productVariantList = [];
        let controller = ProductListController.getInstance();
        controller.fetchProducts(catId).then(resp => { 

            var i;
            for (i = 0; i < resp.length; i++) { 

                let item = new ProductItemStore();
                item.setId(resp[i].id)
                console.log(Utils.setImageWithPlaceholder(resp[i].i));
                item.setImageSource(Utils.setImageWithPlaceholder(resp[i].i))
                item.setTitle(resp[i].t)
                item.setDescription(resp[i].d)
                item.setPrice(resp[i].p)
                item.setDiscountPercentage(resp[i].dp)
                item.setImage2Source(resp[i].i2)
                item.setImage2Source(resp[i].i3)
                item.setImage2Source(resp[i].i4)
                item.setImage2Source(resp[i].i5)
                item.setOpt1(resp[i].opt1)
                item.setOpt2(resp[i].opt2)
                item.setOpt3(resp[i].opt3)
                
                this.productVariantList.push(item);

            }
        })
            .catch(err => console.log(err))
    }


    @action fetctProductListFromSearch(searchString) {
        this.productVariantList = [];
        let controller = ProductListController.getInstance();
        controller.fetchSearchedProducts(searchString).then(resp => {
            console.log("fetchProductListFromSearch in Store " + resp)
            var i;
            for (i = 0; i < resp.length; i++) {
                console.log(Utils.setImageWithPlaceholder(resp[i].image));

                let item = new ProductItemStore();
                item.setId(resp[i].id)
                item.setImageSource(Utils.setImageWithPlaceholder(resp[i].image))
                item.setTitle(resp[i].title)
                item.setDescription(resp[i].short_description)
                item.setPrice(resp[i].price)
                item.setDiscountPercentage(resp[i].discounted_price)
                this.productVariantList.push(item);

            }
        })
            .catch(err => console.log(err))
    }

    @action fetchWishList() {
        this.productVariantList = [];
        let controller = ProductListController.getInstance();
        controller.fetchWishlistProducts().then(resp => {
            console.log("fetchWishList in Store " + resp.items.available)

            let wishlistItems = resp.items.available;
            var i;
            for (i = 0; i < wishlistItems.length; i++) {
                console.log(wishlistItems[i].id, wishlistItems[i].i, wishlistItems[i].t, wishlistItems[i].sd, wishlistItems[i].p, wishlistItems[i].dp);

                let item = new ProductItemStore();
                item.setId(wishlistItems[i].id)
                item.setImageSource(Utils.setImageWithPlaceholder(wishlistItems[i].i))
                item.setTitle(wishlistItems[i].t)
                item.setDescription(wishlistItems[i].sd)
                item.setPrice(wishlistItems[i].p)
                item.setDiscountPercentage(wishlistItems[i].dp)
                this.productVariantList.push(item);

            }
        })
            .catch(err => console.log(err))
    }

    @action setProductList(productVariantList) {
        this.productVariantList = productVariantList;
    }

    @action addProductItem(productItem) {
        this.productVariantList.push(productItem);
    }

    @action removeProductItem(productId) {
        let index = findIndex(productId);

        if (index === -1) {
            return;
        }
        this.productVariantList.remove(index);
    }

    @action updateProductItem(productItem) {
        let index = findIndex(productItem.id);

        this.productVariantList[index].image = productItem.image;
        this.productVariantList[index].price = productItem.price;
        this.productVariantList[index].title = productItem.title;
        this.productVariantList[index].description = productItem.description;
        this.productVariantList[index].discountPrice = productItem.discountPrice;
    }

    findIndex(productId) {
        return this.productVariantList.findIndex(productItem => {
            productItem.id === productId
        })
    }

}

export default new ProductListStore();