import {action, observable} from 'mobx';
import FooterPagesController from "../controllers/FooterPagesController";

export class FooterPagesStore {


    @observable tnc;
    @observable privacyPolicy;
    @observable cancellationAndRefund;

    constructor() {
        this.tnc = "";
        this.privacyPolicy = "";
        this.cancellationAndRefund = "";

    }

    @action fetchData(){
        let footerController = FooterPagesController.getInstance();
        footerController.fetchData().then(resp => {
            //console.log("footer final response", resp[0].terms_and_conditions)
            this.setCancellationAndRefund(resp[0].cancel_and_ref_policy);
            this.setTnC(resp[0].terms_and_conditions);
            this.setPrivacyPolicy(resp[0].privacy_policy);
            console.log("tnc in store ", this.tnc);
        })
            .catch(err => console.log(err))
    }

    @action setTnC(tnc) {
        this.tnc = tnc;
    }

    @action setPrivacyPolicy(privacyPolicy) {
        this.privacyPolicy = privacyPolicy;
    }


    @action setCancellationAndRefund(cancellationAndRefund) {
        this.cancellationAndRefund = cancellationAndRefund;
    }
}


export default new FooterPagesStore();