import {action, observable} from 'mobx';
import ProductItemController from "../controllers/ProductItemController";
import Utils from '../lib/Utils';
import ProductVariantItemStore from "./ProductVariantItemStore";


export default class ProductItemStore {

    @observable productVariantList = [];
    @observable id;
    @observable image;
    @observable image2;
    @observable image3;
    @observable image4;
    @observable image5;
    @observable price;
    @observable title;
    @observable description;
    @observable discountPrice;
    @observable quantity;
    @observable opt1;
    @observable opt2;
    @observable opt3;
    @observable optv1;
    @observable optv2;
    @observable optv3;
    @observable groupid;
    @observable attributeList;
    @observable optionsList;
    @observable options;
    @observable stockKeepingUnit;
    @observable stock;
    @observable cats;
    @observable default;


    constructor() {

        this.id = "";
        this.image = "";
        this.image2 = "";
        this.image3 = "";
        this.image4 = "";
        this.image5 = "";
        this.price = -1;
        this.title = "";
        this.description = "";
        this.discountPrice = -1;
        this.quantity = 0;
        this.opt1 = "";
        this.opt2 = "";
        this.opt3 = "";
        this.optv1 = "";
        this.optv2 = "";
        this.optv3 = "";
        this.groupid = "";
        this.productVariantList = [];
        this.attributeList = [];
        this.optionsList = [];
        this.options = [];
        this.stockKeepingUnit = ""
        this.stock = 0;
        this.cats = "";
        this.default = false;
    }

    @action fetchVariants(id) {

        this.productVariantList = [];
        let controller = ProductItemController.getInstance();
        let catsData = []
        controller.fetchProductVariants(id).then(resp => {

            console.log("Product Variant list mein yea push hoga ", resp.objects)

            var i;
            for (i = 0; i < resp.objects.length; i++) {

                let item = new ProductVariantItemStore();
                for (j = 0; i < resp.objects[i].cats.length; j++) {
                    console.log("lol lol lol store mein cats ka data ", resp.objects[i].cats[j])
                    catsData.push(resp.objects[i].cats[j].id)
                    console.log("lol lol lol store mein catsData ", catsData)
                }

                item.setCats(catsData);
                item.setId(resp.objects[i].id)


                if (resp.objects[i].i != '' && resp.objects[i].i != null) {
                    console.log("Gamma image set karte hue ", this.image)
                    item.setImageSource(resp.objects[i].i)
                } else {
                    item.setImageSource(this.image)
                }

                item.setImage2Source(resp.objects[i].i2);
                item.setImage3Source(resp.objects[i].i3);
                item.setImage4Source(resp.objects[i].i4);
                item.setImage5Source(resp.objects[i].i5);
                item.setTitle(resp.objects[i].t)
                item.setDescription(resp.objects[i].d)
                item.setPrice(resp.objects[i].p)
                item.setDiscountPrice(resp.objects[i].dp)
                item.setStockKeepingUnit(resp.objects[i].sku)
                item.setStock(resp.objects[i].s)
                item.setOpt1(resp.objects[i].opt1)
                item.setOpt2(resp.objects[i].opt2)
                item.setOpt3(resp.objects[i].opt3)
                item.setOptv1(resp.objects[i].optv1)
                item.setOptv2(resp.objects[i].optv2)
                item.setOptv3(resp.objects[i].optv3)
                item.setAttributeList(resp.objects[i].attrs_v)


                if (id === resp.objects[i].id) {
                    item.setDefault(true)
                    this.setImageSource(resp.objects[i].i)
                }


                this.productVariantList.push(item);

            }

            console.log("alpha Product Item Store ke andar: ", this.productVariantList);
        })
            .catch(err => console.log(err))

    }


    @action fetchProduct(id) {
        let controller = ProductItemController.getInstance();
        let catsData = []
        return controller.fetchProduct(id).then(resp => {
            console.log("product data ", resp.objects[0]);
            console.log("lol lol lol store mein cats ", resp.objects[0].cats)

            for (i = 0; i < resp.objects[0].cats.length; i++) {
                console.log("lol lol lol store mein cats ka data ", resp.objects[0].cats[i])
                catsData.push(resp.objects[0].cats[i].id)
                console.log("lol lol lol store mein catsData ", catsData)
            }

            this.setCats(catsData)

            console.log("cats ", this.cats)
            this.setId(resp.objects[0].id);
            this.setImageSource(resp.objects[0].i === null ? "" : resp.objects[0].i);
            this.setImage2Source(resp.objects[0].i2 === null ? "" : resp.objects[0].i2);
            this.setImage3Source(resp.objects[0].i3 === null ? "" : resp.objects[0].i3);
            this.setImage4Source(resp.objects[0].i4 === null ? "" : resp.objects[0].i4);
            this.setImage5Source(resp.objects[0].i5 === null ? "" : resp.objects[0].i5);
            this.setPrice(resp.objects[0].p);
            this.setDescription(resp.objects[0].d);
            this.setTitle(resp.objects[0].t);
            this.setOpt1(resp.objects[0].opt1);
            this.setOpt2(resp.objects[0].opt2);
            this.setOpt3(resp.objects[0].opt3);
            this.setOptv1(resp.objects[0].optv1)
            this.setOptv2(resp.objects[0].optv2)
            this.setOptv3(resp.objects[0].optv3)
            this.setGroupId(resp.objects[0].g);
            this.setAttributeList(resp.objects[0].attrs_v)
            this.setDiscountPrice(resp.objects[0].dp)
            this.setStock(resp.objects[0].s)
            this.setStockKeepingUnit(resp.objects[0].sku)

            return resp;

        })
            .catch(err => console.log(err))
    }


    @action setProductFavouite(id, userId) {
        return new Promise((resolve, reject) => {
            let controller = ProductItemController.getInstance();
            console.log(" wishlist data request initiated", id);
            controller.setFavrouiteProduct(id, userId).then(resp => {
                console.log("product wishlist data ", resp);
                resolve(resp)
            })
                .catch(err => console.log(err))
        })

    }

    @action updateProductWithVariant(variant) {
        console.log("Item store ke andar Variant ", variant)
        this.setId(variant.id);

        if (variant.image != null && variant.image != '') {
            this.setImageSource(variant.image);
        }

        if (variant.image2 != null && variant.image2 != '') {
            this.setImage2Source(variant.image2);
        }

        if (variant.image3 != null && variant.image3 != '') {
            this.setImage3Source(variant.image3);
        }

        if (variant.image4 != null && variant.image4 != '') {
            this.setImage4Source(variant.image4);
        }

        if (variant.image5 != null && variant.image5 != '') {
            this.setImage5Source(variant.image5);
        }


        this.setPrice(variant.price);
        this.setDescription(variant.description);
        this.setTitle(variant.title);
        // this.setOpt1(variant.opt1);
        // this.setOpt2(variant.opt2);
        // this.setOpt3(variant.opt3);
        this.setOptv1(variant.optv1);
        this.setOptv2(variant.optv2);
        this.setOptv3(variant.optv3);
        this.setGroupId(variant.groupid);
        this.setAttributeList(variant.attributeList)
        this.setDiscountPrice(variant.discountPrice)
        this.setStock(variant.stock)
        this.setStockKeepingUnit(variant.stockKeepingUnit)


        console.log("Variant update hone ke baad attributes ", variant.attrs_v, this.title, this.attributeList)
    }

    @action fetchProductOptions(prodId) {
        let controller = ProductItemController.getInstance();

        return controller.fetchProductOptions(prodId)
            .then((data) => {
                console.log("Product Options Ka data ", data.objects)

                if (data.objects.length > 0) {
                    data.objects.map((item) => {
                        this.optionsList.push(item)
                    })
                }

                console.log("Options List ", this.optionsList);
            })
            .catch((err) => {
                console.log("Product Options ka error ", err)
            })
    }


    @action shippingOptions(country, pincode, prodId) {

        let controller = ProductItemController.getInstance();

        console.log("Check Delivery Store")
        return new Promise((resolve, reject) => {


            controller.shippingOptions(country, pincode, prodId).then((data) => {
                console.log("Shipping options in stores ", data)
                resolve(data)
            })
        })
    }

    @action setData(data) {
        this.id = data.id;
        this.image = data.image;
        this.image2 = data.image2;
        this.image3 = data.image3;
        this.image4 = data.image4;
        this.image5 = data.image5;
        this.price = data.price;
        this.description = data.description;
        this.discountPercentage = data.discountPercentage;
        this.opt1 = data.opt1;
        this.opt2 = data.opt2;
        this.opt3 = data.opt3;
        this.groupid = data.groupid;
        this.optv1 = data.optv1;


    }

    @action setVariantList(data) {
        this.productVariantList.push(data)
    }

    @action setStockKeepingUnit(data) {
        this.stockKeepingUnit = data
    }

    @action setStock(data) {
        this.stock = data
    }

    @action setAttributeList(data) {
        this.attributeList = []
        console.log("Product Attributes ", data)
        this.attributeList.push(data)
        console.log("Product Attributes List data ", this.attributeList)
    }

    @action setId(id) {
        this.id = id;
    }

    @action setCats(cats) {
        console.log("lol lol lol store mein cats set karte hue ", cats)
        this.cats = cats.join(',')
        console.log("lol lol lol store mein cats set karne ke baad ", this.cats)
    }

    @action setQuantity(quantity) {
        this.quantity = quantity;
    }


    @action setImageSource(imageSource) {
        this.image = imageSource;
    }

    @action setImage2Source(imageSource2) {
        this.image2 = imageSource2;
    }

    @action setImage3Source(imageSource3) {
        this.image3 = imageSource3;
    }

    @action setImage4Source(imageSource4) {
        this.image4 = imageSource4;
    }

    @action setImage5Source(imageSource5) {
        this.image5 = imageSource5;
    }

    @action setPrice(price) {
        this.price = price;
    }

    @action setDefault(bool) {
        this.default = bool
    }

    @action setTitle(title) {
        this.title = title;
    }

    @action setDescription(description) {
        this.description = description;
    }

    @action setDiscountPrice(discount) {
        this.discountPrice = discount;
    }

    @action setOpt1(opt1) {
        this.opt1 = opt1;
    }

    @action setOpt2(opt2) {
        this.opt2 = opt2;
    }

    @action setOpt3(opt3) {
        this.opt3 = opt3;
    }

    @action setOptv1(optv1) {
        this.optv1 = optv1;
    }

    @action setOptv2(optv2) {
        this.optv2 = optv2;
    }

    @action setOptv3(optv3) {
        this.optv3 = optv3;
    }


    @action setGroupId(groupid) {
        this.groupid = groupid;
    }

    @action setVariantList(variantList) {
        this.variantList = variantList;
    }

    @action setProductOptions(options) {
        options.map((item) => {
            this.options.push(item)
        })

        console.log("Product item store mein product options (PARAMETER) ", options)
        console.log("Product item store mein product options ", this.options)
    }
}