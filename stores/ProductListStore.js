import { action, observable } from 'mobx';
import ProductListController from '../controllers/ProductListController';
import ProductItemStore from './ProductItemStore';
import Utils from "../lib/Utils";
import SQLite from 'react-native-sqlite-storage'
import {AsyncStorage} from 'react-native'

export class ProductListStore {


    @observable productList;


    constructor() {
        this.productList = [];
        this.nextUrl = ''
        this.enduser_type = ''

        global.storage.load({
            key: 'endUserType'
        }).then((resp) => {
            this.enduser_type = resp.endUserType
            console.log("this.enduser_type storage ", resp.endUserType)
        })

        console.log("this.enduser_type", this.enduser_type)
    }



    @action updateProductList(catId) {

        return new Promise((resolve, reject) => {
            let controller = ProductListController.getInstance();
            controller.fetchProducts(catId, this.nextUrl).then(resp => {
                console.log("next url ", this.nextUrl)
                this.nextUrl = resp.meta.next
                if (this.nextUrl === null) {
                    resolve(true)
                    return
                }


                var i;
                for (i = 0; i < resp.objects.length; i++) {

                    let item = new ProductItemStore();
                    item.setId(resp.objects[i].id)
                    console.log(Utils.setImageWithPlaceholder(resp.objects[i].i));
                    item.setImageSource(resp.objects[i].i)
                    item.setTitle(resp.objects[i].t)
                    item.setDescription(resp.objects[i].d)
                    item.setPrice(resp.objects[i].p)
                    item.setDiscountPrice(resp.objects[i].dp)
                    item.setImage2Source(resp.objects[i].i2)
                    item.setImage2Source(resp.objects[i].i3)
                    item.setImage2Source(resp.objects[i].i4)
                    item.setImage2Source(resp.objects[i].i5)
                    item.setOpt1(resp.objects[i].opt1)
                    item.setOpt2(resp.objects[i].opt2)
                    item.setOpt3(resp.objects[i].opt3)
                    item.setOptv1(resp.objects[i].optv1)
                    item.setOptv2(resp.objects[i].optv2)
                    item.setOptv3(resp.objects[i].optv3)
                    item.setAttributeList(resp.objects[i].attrs_v)
                    item.setStockKeepingUnit(resp.objects[i].sku)

                    this.productList.push(item);

                }

                console.log("Next product url ", this.nextUrl)

                resolve(true)
            })
                .catch(err => console.log("error prod list fetch ", err))
        })


    }

    @action fetctProductList(catId, nextUrl, limit) {
        console.log("fetch product list cat id ", catId)
        this.productList = [];
        let controller = ProductListController.getInstance();
        controller.fetchProducts(catId, nextUrl, limit).then(resp => {

            this.nextUrl = resp.meta.next

            var i;
            for (i = 0; i < resp.objects.length; i++) {

                let item = new ProductItemStore();
                item.setId(resp.objects[i].id)
                console.log(Utils.setImageWithPlaceholder(resp.objects[i].i));
                item.setImageSource(resp.objects[i].i)
                item.setTitle(resp.objects[i].t)
                item.setDescription(resp.objects[i].d)
                item.setPrice(resp.objects[i].p)
                item.setDiscountPrice(resp.objects[i].dp)
                item.setImage2Source(resp.objects[i].i2)
                item.setImage2Source(resp.objects[i].i3)
                item.setImage2Source(resp.objects[i].i4)
                item.setImage2Source(resp.objects[i].i5)
                item.setOpt1(resp.objects[i].opt1)
                item.setOpt2(resp.objects[i].opt2)
                item.setOpt3(resp.objects[i].opt3)
                item.setOptv1(resp.objects[i].optv1)
                item.setOptv2(resp.objects[i].optv2)
                item.setOptv3(resp.objects[i].optv3)
                item.setAttributeList(resp.objects[i].attrs_v)
                item.setStockKeepingUnit(resp.objects[i].sku)
                item.setGroupId(resp.objects[i].g)

                this.productList.push(item);

            }

            console.log("Next product url ", this.nextUrl)
        })
            .catch(err => console.log(err))
    }

    @action applyFilter(catId, filterQuery) {
        this.productList = [];
        let controller = ProductListController.getInstance();
        controller.applyFilter(catId, filterQuery).then((data) => {
            console.log("Filters Apply hone ke baad (STORE) ", data.objects)

            for (i = 0; i < data.objects.length; i++) {

                let item = new ProductItemStore();
                item.setId(data.objects[i].id)
                console.log(Utils.setImageWithPlaceholder(data.objects[i].i));
                item.setImageSource(data.objects[i].i)
                item.setTitle(data.objects[i].t)
                item.setDescription(data.objects[i].d)
                item.setPrice(data.objects[i].p)
                item.setDiscountPrice(data.objects[i].dp)
                item.setImage2Source(data.objects[i].i2)
                item.setImage2Source(data.objects[i].i3)
                item.setImage2Source(data.objects[i].i4)
                item.setImage2Source(data.objects[i].i5)
                item.setOpt1(data.objects[i].opt1)
                item.setOpt2(data.objects[i].opt2)
                item.setOpt3(data.objects[i].opt3)
                item.setAttributeList(data.objects[i].attrs_v)

                this.productList.push(item);

            }
        })
    }

    @action fetchAttributes(catId) {
        const db = SQLite.openDatabase("enduser.db", "1.0", "Demo", -1);
        db.transaction((txn) => {
            txn.executeSql('DROP TABLE IF EXISTS AttributeKeys', []);
            txn.executeSql('CREATE TABLE IF NOT EXISTS AttributeKeys(KEY_NAME VARCHAR(30), KEY_GROUP_ID VARCHAR(30), KEY_SORT  VARCHAR(10), KEY_ATTR_KEY_ID  VARCHAR(10), KEY_FILTERABLE BIT, KEY_ATTR_TYPE VARCHAR(30) NOT NULL)', []);
            txn.executeSql('DROP TABLE IF EXISTS AttributeValues', []);
            txn.executeSql('CREATE TABLE IF NOT EXISTS AttributeValues(KEY_ATTR_KEY_ID VARCHAR(30), KEY_ATTR_VALUE_ID VARCHAR(30), KEY_VALUE  VARCHAR(10), KEY_PROD_ID  VARCHAR(10))', []);
            txn.executeSql('DROP TABLE IF EXISTS AttributeGroups', []);
            txn.executeSql('CREATE TABLE IF NOT EXISTS AttributeGroups(KEY_NAME VARCHAR(30), KEY_GROUP_ID VARCHAR(30), KEY_SORT  VARCHAR(10), KEY_CAT_ID  VARCHAR(10))', []);
        })
        let controller = ProductListController.getInstance();
        console.log("cat ID in fetch attributes ", catId)
        controller.fetchAttributes(catId)
            .then(data => {
                if (data.length > 0) {
                    data.map((item) => {
                        console.log("map karne ke baad attributes ", item)
                        db.transaction((txn) => {
                            console.log("Database ke andar")
                            //Attribute Keys

                            txn.executeSql('INSERT INTO AttributeKeys (KEY_NAME, KEY_GROUP_ID, KEY_SORT, KEY_ATTR_KEY_ID, KEY_FILTERABLE, KEY_ATTR_TYPE) VALUES (:KEY_NAME, :KEY_GROUP_ID, :KEY_SORT, :KEY_ATTR_KEY_ID, :KEY_FILTERABLE, :KEY_ATTR_TYPE)', [item.n, item.kgp.id, item.so, item.id, item.f, item.ft]);
                            txn.executeSql('SELECT * FROM `AttributeKeys`', [], function (tx, res) {
                                for (let i = 0; i < res.rows.length; ++i) {
                                    console.log('Attribute Key Items: ', res.rows.item(i));
                                }
                            });
                            //Attribute Values
                            if (item.kvs.length > 0) {
                                console.log("VALUES LENGTH ", item.kvs.length)
                                for (i = 0; i < item.kvs.length; i++) {
                                    console.log("Value ke loop ke andar ", i)
                                    txn.executeSql('INSERT INTO AttributeValues (KEY_ATTR_KEY_ID, KEY_ATTR_VALUE_ID, KEY_VALUE) VALUES (:KEY_ATTR_KEY_ID, :KEY_ATTR_VALUE_ID, :KEY_VALUE)', [item.id, item.kvs[i].id, item.kvs[i].v]);


                                }

                                txn.executeSql('SELECT * FROM `AttributeValues`', [], function (tx, res) {
                                    for (let i = 0; i < res.rows.length; ++i) {
                                        console.log('Attribute Value Items: ', res.rows.item(i));
                                    }


                                });


                            }

                            if (item.kgp) {
                                console.log("GROUPS LENGTH ", item.kgp.length)

                                txn.executeSql('INSERT INTO AttributeGroups (KEY_NAME, KEY_GROUP_ID, KEY_SORT) VALUES (:KEY_NAME, :KEY_GROUP_ID, :KEY_SORT)', [item.kgp.n, item.kgp.id, item.kgp.so]);
                                txn.executeSql('SELECT * FROM `AttributeGroups`', [], function (tx, res) {
                                    for (let i = 0; i < res.rows.length; ++i) {
                                        console.log('Attribute Group Items: ', res.rows.item(i));
                                    }


                                });


                            }

                        })
                    })
                }
            })

    }

    @action fetchFeaturedProducts(limit) {
        this.productList = [];
        let controller = ProductListController.getInstance();

        controller.fetchFeaturedProducts(limit, null)
            .then((data) => {
                console.log("Featured Products in Store ", data.objects)
                console.log("Featured Products ETP data ", data.etp_data)

                this.nextUrl = data.meta.next

                var i;
                for (i = 0; i < data.objects.length; i++) {

                    let item = new ProductItemStore();
                    item.setId(data.objects[i].id)
                    item.setImageSource(data.objects[i].i)
                    item.setTitle(data.objects[i].t)
                    // (this.enduser_type != '' ) ? item.setPrice(data.etp_data[data.objects[i].id][this.enduser_type]) : item.setPrice(data.objects[i].p)
                    item.setPrice(data.objects[i].p)
                    item.setDiscountPrice(data.objects[i].dp)


                    this.productList.push(item);

                }

                console.log("Next product url ", this.nextUrl)

            })
            .catch((err) => {
                console.log("Err in featured products ", err)
            })

    }

    @action fetchLatestProducts(limit) {
        this.productList = [];
        let controller = ProductListController.getInstance();

        controller.fetchLatestProducts(limit, null)
            .then((data) => {
                console.log("Latest Products in Store ", data.objects)
                console.log("Latest Products ETP data ", data.etp_data)

                this.nextUrl = data.meta.next

                var i;
                for (i = 0; i < data.objects.length; i++) {

                    let item = new ProductItemStore();
                    item.setId(data.objects[i].id)
                    item.setImageSource(data.objects[i].i)
                    item.setTitle(data.objects[i].t)
                    // (this.enduser_type != '' ) ? item.setPrice(data.etp_data[data.objects[i].id][this.enduser_type]) : item.setPrice(data.objects[i].p)
                    item.setPrice(data.objects[i].p)
                    item.setDiscountPrice(data.objects[i].dp)


                    this.productList.push(item);

                }

                console.log("Next product url ", this.nextUrl)

            })
            .catch((err) => {
                console.log("Err in featured products ", err)
            })

    }


    @action fetctProductListFromSearch(searchString) {
        this.productList = [];

        return new Promise((resolve, reject) => {
            let controller = ProductListController.getInstance();
            controller.fetchSearchedProducts(searchString).then(resp => {
                console.log("fetchProductListFromSearch in Store ", resp.results)

                this.nextUrl = resp.meta.next

                var i;
                for (i = 0; i < resp.results.length; i++) {


                    let item = new ProductItemStore();
                    item.setId(resp.results[i].id)
                    item.setImageSource(resp.results[i].image)
                    item.setTitle(resp.results[i].title)
                    item.setDescription(resp.results[i].short_description)
                    item.setPrice(resp.results[i].price)
                    item.setDiscountPrice(resp.results[i].discounted_price)
                    this.productList.push(item);

                }

                console.log("Next product url ", this.nextUrl)

                resolve(true)
            })
                .catch(err => console.log(err))
        })

    }

    @action fetchWishList(id) {
        console.log("Product list store k andar userId ", id);
        this.productList = [];
        return new Promise((resolve, reject) => {
            let controller = ProductListController.getInstance();
            controller.fetchWishlistProducts(id).then(resp => {
                console.log("fetchWishList in Store " + resp.items.available)

                let wishlistItems = resp.items.available;
                var i;
                for (i = 0; i < wishlistItems.length; i++) {
                    console.log(wishlistItems[i].id, wishlistItems[i].i, wishlistItems[i].t, wishlistItems[i].sd, wishlistItems[i].p, wishlistItems[i].dp);

                    let item = new ProductItemStore();
                    item.setId(wishlistItems[i].id)
                    item.setImageSource(wishlistItems[i].i)
                    item.setTitle(wishlistItems[i].t)
                    item.setDescription(wishlistItems[i].sd)
                    item.setPrice(wishlistItems[i].p)
                    item.setDiscountPrice(wishlistItems[i].dp)
                    this.productList.push(item);

                    console.log("Product list after wish list ", this.productList);

                    resolve(true)
                }
            })
                .catch(err => {
                    reject(false)
                })
        })

    }

    @action setProductList(productList) {
        this.productList = productList;
    }

    @action addProductItem(productItem) {
        this.productList.push(productItem);
    }

    @action removeProductItem(productId) {
        let index = findIndex(productId);

        if (index === -1) {
            return;
        }
        this.productList.remove(index);
    }

    @action updateProductItem(productItem) {
        let index = findIndex(productItem.id);

        this.productList[index].image = productItem.image;
        this.productList[index].price = productItem.price;
        this.productList[index].title = productItem.title;
        this.productList[index].description = productItem.description;
        this.productList[index].discountPrice = productItem.discountPrice;
    }

    findIndex(productId) {
        return this.productList.findIndex(productItem => {
            productItem.id === productId
        })
    }

}

export default ProductListStore;