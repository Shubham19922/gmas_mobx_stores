import {action, observable} from 'mobx';
import AddressItemController from "../controllers/AddressItemController";

export default class AddressItemStore {


    @observable id;
    @observable code;
    @observable firstname;
    @observable lastname;
    @observable countryDisplay;
    @observable address;
    @observable stateDisplay;
    @observable city;
    @observable country;
    @observable state;
    @observable contact;
    @observable email;


    constructor() {

        this.id = "";
        this.firstname = "";
        this.lastname = "";
        this.countryDisplay = "";
        this.address = "";
        this.stateDisplay = "";
        this.city = "";
        this.country = "";
        this.state = "";
        this.contact = "";
        this.email = "";
        this.code = "";
    }


    @action fetchAddress(id) {
        let controller = AddressItemController.getInstance();
        controller.fetchAddress(id).then(resp => {
            console.log("product data", resp);
            this.setId(resp[0].id);
            this.setFirstName(resp[0].firstname);
            this.setLastName(resp[0].lastname);
            this.setAddress(resp[0].address);
            this.setCity(resp[0].city);
            this.setCountryDisplay(resp[0].country_display);
            this.setCountry(resp[0].country);
            this.setState(resp[0].state);
            this.setStateDisplay(resp[0].state_display);
            this.setContact(resp[0].contact);
            this.setEmail(resp[0].email);
            this.setCode(resp[0].code);
        })
            .catch(err => console.log(err))
    }

    @action setId(id) {
        this.id = id;
    }

    @action setFirstName(firstName) {
        this.firstname = firstName;
    }


    @action setLastName(lastName) {
        this.lastname = lastName;
    }

    @action setCountryDisplay(countryDisplay) {
        this.countryDisplay = countryDisplay;
    }

    @action setAddress(address) {
        this.address = address;
    }

    @action setStateDisplay(stateDisplay) {
        this.stateDisplay = stateDisplay;
    }

    @action setCity(city) {
        this.city= city;
    }

    @action setCountry(country) {
        this.country= country;
    }

    @action setContact(contact) {
        this.contact = contact;
    }

    @action setEmail(email) {
        this.email = email;
    }

    @action setState(state) {
        this.state = state;
    }

    @action setCode(code) {
        this.code = code;
    }
}
